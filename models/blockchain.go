package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Blockchain struct {
	ID              primitive.ObjectID `json:"_id" bson:"_id"`
	Address         string             `bson:"address" json:"address"`
	TransactionHash string             `bson:"transaction_hash" json:"transaction_hash"`
}
