package errors

import (
	"github.com/pkg/errors"
	"google.golang.org/grpc/status"
)

// Define Errors
var (
	ErrDatabase            = status.New(206001, "database error").Err()
	ErrMarshal             = status.New(206002, "marshal failed").Err()
	ErrUnmarshal           = status.New(206003, "unmarshal failed").Err()
	ErrInternalServerError = status.New(206004, "internal server error").Err()
	ErrBadRequest          = status.New(206005, "bad request").Err()
	ErrSystemError         = status.New(206006, "system error").Err()
	ErrInvalidIdToken      = status.New(206007, "token id not found").Err()
	ErrNotFoundCategory    = status.New(206008, "category not found").Err()
	ErrCheckAccessKey      = status.New(206009, "access key is not valid").Err()
)

// ErrorWithMessage wraps detail error
func ErrorWithMessage(err error, message string) error {
	s, ok := status.FromError(err)
	if !ok {
		return errors.Wrap(err, message)
	}

	grpcStatus := status.New(s.Code(), s.Message()+": "+message)
	return grpcStatus.Err()
}
