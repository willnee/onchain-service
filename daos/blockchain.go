package daos

import (
	"github.com/pkg/errors"
	"gitlab.com/willnee/onchain-service/helpers"
	"gitlab.com/willnee/onchain-service/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CreateMedia : Add a new media
func (d *DAO) Mint(art *models.Blockchain) error {
	return nil
}

// GetMedia
func (d *DAO) GetNFT(queries bson.M, opts *options.FindOneOptions) (art *models.Blockchain, err error) {
	ctx, cancel := helpers.NewCtx()
	defer cancel()

	if err = d.BlockchainColl.FindOne(ctx, queries, opts).Decode(&art); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, nil
		}
		return nil, errors.Wrap(err, "MongoDB.FindOne")
	}

	return
}

// FindNFTByQuery : ...
func (d *DAO) FindNFTByQuery(queries bson.M, opts *options.FindOptions) (blockchains []*models.Blockchain, err error) {
	ctx, cancel := helpers.NewCtx()
	defer cancel()

	cur, err := d.BlockchainColl.Find(ctx, queries, opts)
	if err != nil {
		return nil, errors.Wrap(err, "MongoDB.FindNFTByQuery.Find")
	}
	defer cur.Close(ctx)
	for cur.Next(ctx) {
		var blockchain models.Blockchain
		err := cur.Decode(&blockchain)
		if err != nil {
			return nil, errors.Wrap(err, "MongoDB.FindNFTByQuery.Decode")
		}
		blockchains = append(blockchains, &blockchain)
	}

	if err := cur.Err(); err != nil {
		return nil, errors.Wrap(err, "MongoDB.FindNFTByQuery.cur")
	}

	return
}
