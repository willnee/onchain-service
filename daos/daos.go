package daos

import (
	"context"

	"time"

	"github.com/pkg/errors"
	"gitlab.com/willnee/onchain-service/config"
	"gitlab.com/willnee/onchain-service/database"
	"gitlab.com/willnee/onchain-service/helpers"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

// DAO : struct
type DAO struct {
	Client         *mongo.Client
	BlockchainColl *mongo.Collection
}

// NewDAO with config
func NewDAO(conf *config.Config) (*DAO, error) {
	var err error

	db, err := database.InitDB(conf)
	if err != nil {
		return nil, errors.Wrap(err, "database.Init")
	}

	db.CreateCollection(context.TODO(), database.BlockchainColl)

	return &DAO{
		Client:         db.Client(),
		BlockchainColl: db.Collection(database.BlockchainColl),
	}, nil
}

// WithTransaction : callback
func (d *DAO) WithTransaction(callback func(mongo.SessionContext) (interface{}, error)) error {
	ctx, cancel := helpers.NewCtx()
	defer cancel()

	wc := writeconcern.New(writeconcern.WMajority(), writeconcern.WTimeout(1*time.Second))
	rc := readconcern.Snapshot()
	sessionOpts := options.Session().SetDefaultWriteConcern(wc).SetDefaultReadConcern(rc)

	session, err := d.Client.StartSession(sessionOpts)
	if err != nil {
		return err
	}

	defer session.EndSession(ctx)

	_, err = session.WithTransaction(ctx, callback)
	if err != nil {
		return err
	}

	return nil
}
