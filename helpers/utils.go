package helpers

import (
	"context"
	"fmt"
	"time"

	"cloud.google.com/go/storage"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/api/option"
)

func NewCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 6*time.Second)
}
func HashPassword(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(bytes)
}

func CheckValidHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

const ImagePath = "../images/"
const MetaPath = "metadata/"

type StorageSvc struct {
	bucketName string
}

func NewStorageService() *StorageSvc {
	return &StorageSvc{
		bucketName: "test-hoki",
	}
}

func (s *StorageSvc) UploadFile(file []byte, name string) (string, error) {

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("./config/storage.json"))
	if err != nil {
		return "", err
	}
	fmt.Println(s.bucketName)

	obj := client.Bucket(s.bucketName).Object(string("metadata/") + name)
	wc := obj.NewWriter(ctx)
	wc.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	if err != nil {
		return "", err
	}
	if _, err := wc.Write(file); err != nil {

		return "", err
	}
	if err := wc.Close(); err != nil {
		fmt.Println(err)
		return "", err
	}
	url := "https://storage.googleapis.com/" + s.bucketName + "/" + wc.Attrs().Name
	fmt.Println(url)
	return url, nil
}
