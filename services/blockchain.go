package services

import (
	"gitlab.com/willnee/onchain-service/config"
	"gitlab.com/willnee/onchain-service/daos"
)

type BlockchainSvc struct {
	dao  *daos.DAO
	conf *config.Config
}

func NewBlockchainSvc(dao *daos.DAO, conf *config.Config) *BlockchainSvc {
	return &BlockchainSvc{dao: dao, conf: conf}
}

func (s *BlockchainSvc) Mint(req)
