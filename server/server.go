package server

import (
	pb "gitlab.com/peahokinet/blockchain/travel-nft/protobuf/gen/go"
	"gitlab.com/willnee/onchain-service/services"
)

type BlockchainSrv struct {
	pb.UnimplementedUserSrvServer
	BlockchainSrv *services.BlockchainSvc
}

func NewBlockchainSrv(BlockchainSvc *services.BlockchainSvc) *BlockchainSrv {
	return &BlockchainSrv{
		BlockchainSrv: BlockchainSvc,
	}
}
