package database

import (
	"context"
	"time"

	"gitlab.com/willnee/onchain-service/config"
	"gitlab.com/willnee/onchain-service/helpers"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	BlockchainColl string = "blockchains"
)

// InitDB :
func InitDB(conf *config.Config) (*mongo.Database, error) {
	clientOptions := options.Client().SetMaxConnIdleTime(5 * time.Second).SetMaxPoolSize(250).ApplyURI(conf.DB).SetRegistry(bigIntMongoRegistry)

	ctx := context.Background()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	db := client.Database(conf.DBName)

	if err = CreateIndexes(db); err != nil {
		return nil, err
	}

	return db, nil
}

// CreateIndexes :
func CreateIndexes(db *mongo.Database) error {
	roundCollection := db.Collection(BlockchainColl)

	indexes := []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "_id", Value: 1},
				{Key: "address", Value: 1},
			},
		},
	}

	ctx, cancel := helpers.NewCtx()
	defer cancel()
	_, err := roundCollection.Indexes().CreateMany(ctx, indexes)
	if err != nil {
		return err
	}

	return nil
}
