package main

import (
	"fmt"
	"log"
	"net"

	_ "github.com/go-sql-driver/mysql"

	pb "gitlab.com/peahokinet/blockchain/travel-nft/protobuf/gen/go"
	"gitlab.com/willnee/onchain-service/config"
	"gitlab.com/willnee/onchain-service/daos"
	"gitlab.com/willnee/onchain-service/server"
	"gitlab.com/willnee/onchain-service/services"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	_ "go.uber.org/automaxprocs"
)

const (
	// EnvProduction :
	EnvProduction = "production"
)

var (
	conf          *config.Config
	logger        *zap.Logger
	blockchainSvc *services.BlockchainSvc
)

func init() {
	conf = config.GetConfig()

	// init log
	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatalf("failed to create zap logger: %v", err)
	}
	zap.ReplaceGlobals(logger)
	defer logger.Sync()

	dao, err := daos.NewDAO(conf)
	if err != nil {
		logger.Fatal("failed to init daos", zap.Error(err))
	}

	blockchainSvc = services.NewBlockchainSvc(dao, conf)
}

func main() {

	var (
		opts []grpc.ServerOption
	)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Port))
	if err != nil {
		logger.Fatal("failed to listen", zap.Error(err))
	}

	grpcServer := grpc.NewServer(opts...)
	pb.RegisterOnchainSrvServer(grpcServer, server.NewBlockchainSrv(blockchainSvc))
	grpcServer.Serve(lis)
}
