FROM golang:1.17

WORKDIR /usr/src/app

# COPY go.mod go.sum ./
# RUN go mod download && go mod verify

COPY ./config/conf_staging.yaml ./config/conf.yaml 
COPY main .
RUN [ ! -d "../images" ] && mkdir ../images

CMD ./main